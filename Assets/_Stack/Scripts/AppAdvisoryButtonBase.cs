﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base for all button script
/// </summary>
public class AppAdvisoryButtonBase : MonoBehaviour 
{
	public virtual void OnClicked(){}
}
